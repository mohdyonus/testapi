Feature: Categories Details (API) Validation

@API
Scenario Outline: Validate the Fields and its value
	Given The Categories Detail API
	When Call the requested API get service
	And Returns the Successful response 
	Then Validate API response have Element "<Fields>" as "<Value>"
	Examples:
	|Fields|Value|
	|Name|Carbon credits|
	|CanRelist|true|
	

@API	
Scenario:  Validate the Promotions
	Given The Categories Detail API
	When Call the requested API get service
	And Returns the Successful response 
	Then Validate API response have below Elements in "Promotions"
	|Field|Value|
	|Name|Gallery|
	|Description|2x larger image|


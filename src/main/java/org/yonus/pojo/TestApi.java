package org.yonus.pojo;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class TestApi
{
	
	@JsonProperty("MaximumTitleLength")	
private String MaximumTitleLength;
	@JsonProperty("IsFreeToRelist")
private String IsFreeToRelist;
	@JsonProperty("DefaultRelistDuration")
private String DefaultRelistDuration;
	@JsonProperty("AreaOfBusiness")
private String AreaOfBusiness;
	@JsonProperty("Promotions")
private List<Promotions> Promotions;
	@JsonProperty("CanRelist")
private String CanRelist;
	@JsonProperty("LegalNotice")
private String LegalNotice;
	@JsonProperty("Path")
private String Path;
@JsonProperty("Name")
private String Name;
@JsonProperty("Fees")
private Fees Fees;
@JsonProperty("CanListAuctions")
private String CanListAuctions;

@JsonProperty("CategoryId")
private String CategoryId;

@JsonProperty("CanListClassifieds")
private String CanListClassifieds;
@JsonProperty("DefaultDuration")
private String DefaultDuration;
@JsonProperty("EmbeddedContentOptions")
private String[] EmbeddedContentOptions;
@JsonProperty("FreePhotoCount")
private String FreePhotoCount;
@JsonProperty("MaximumPhotoCount")
private String MaximumPhotoCount;
@JsonProperty("AllowedDurations")
private String[] AllowedDurations;


public String getMaximumTitleLength ()
{
return MaximumTitleLength;
}

public void setMaximumTitleLength (String MaximumTitleLength)
{
this.MaximumTitleLength = MaximumTitleLength;
}

public String getIsFreeToRelist ()
{
return IsFreeToRelist;
}

public void setIsFreeToRelist (String IsFreeToRelist)
{
this.IsFreeToRelist = IsFreeToRelist;
}

public String getDefaultRelistDuration ()
{
return DefaultRelistDuration;
}

public void setDefaultRelistDuration (String DefaultRelistDuration)
{
this.DefaultRelistDuration = DefaultRelistDuration;
}

public String getAreaOfBusiness ()
{
return AreaOfBusiness;
}

public void setAreaOfBusiness (String AreaOfBusiness)
{
this.AreaOfBusiness = AreaOfBusiness;
}

public List getPromotions ()
{
return Promotions;
}

public void setPromotions (List<Promotions> Promotions)
{
this.Promotions = Promotions;
}

public String getCanRelist ()
{
return CanRelist;
}

public void setCanRelist (String CanRelist)
{
this.CanRelist = CanRelist;
}

public String getLegalNotice ()
{
return LegalNotice;
}

public void setLegalNotice (String LegalNotice)
{
this.LegalNotice = LegalNotice;
}

public String getPath ()
{
return Path;
}

public void setPath (String Path)
{
this.Path = Path;
}

public String getName ()
{
return Name;
}

public void setName (String Name)
{
this.Name = Name;
}

public Fees getFees ()
{
return Fees;
}

public void setFees (Fees Fees)
{
this.Fees = Fees;
}

public String getCanListAuctions ()
{
return CanListAuctions;
}

public void setCanListAuctions (String CanListAuctions)
{
this.CanListAuctions = CanListAuctions;
}

public String getCategoryId ()
{
return CategoryId;
}

public void setCategoryId (String CategoryId)
{
this.CategoryId = CategoryId;
}

public String getCanListClassifieds ()
{
return CanListClassifieds;
}

public void setCanListClassifieds (String CanListClassifieds)
{
this.CanListClassifieds = CanListClassifieds;
}

public String getDefaultDuration ()
{
return DefaultDuration;
}

public void setDefaultDuration (String DefaultDuration)
{
this.DefaultDuration = DefaultDuration;
}

public String[] getEmbeddedContentOptions ()
{
return EmbeddedContentOptions;
}

public void setEmbeddedContentOptions (String[] EmbeddedContentOptions)
{
this.EmbeddedContentOptions = EmbeddedContentOptions;
}

public String getFreePhotoCount ()
{
return FreePhotoCount;
}

public void setFreePhotoCount (String FreePhotoCount)
{
this.FreePhotoCount = FreePhotoCount;
}

public String getMaximumPhotoCount ()
{
return MaximumPhotoCount;
}

public void setMaximumPhotoCount (String MaximumPhotoCount)
{
this.MaximumPhotoCount = MaximumPhotoCount;
}

public String[] getAllowedDurations ()
{
return AllowedDurations;
}

public void setAllowedDurations (String[] AllowedDurations)
{
this.AllowedDurations = AllowedDurations;
}

@Override
public String toString()
{
return "ClassPojo [ CategoryId = "+CategoryId+",Name = "+Name+", MaximumTitleLength = "+MaximumTitleLength+", IsFreeToRelist = "+IsFreeToRelist+", DefaultRelistDuration = "+DefaultRelistDuration+", AreaOfBusiness = "+AreaOfBusiness+", Promotions = "+Promotions+", CanRelist = "+CanRelist+", LegalNotice = "+LegalNotice+", Path = "+Path+",  Fees = "+Fees+", CanListAuctions = "+CanListAuctions+", CanListClassifieds = "+CanListClassifieds+", DefaultDuration = "+DefaultDuration+", EmbeddedContentOptions = "+EmbeddedContentOptions+", FreePhotoCount = "+FreePhotoCount+", MaximumPhotoCount = "+MaximumPhotoCount+", AllowedDurations = "+AllowedDurations+"]";
}
}

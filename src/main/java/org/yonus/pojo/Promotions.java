package org.yonus.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

public class Promotions
{
	@JsonProperty("Name")
 private String Name;
	@JsonProperty("Description")
 private String Description;
	@JsonProperty("Price")
 private String Price;
	@JsonProperty("Id")
 private String Id;
	@JsonProperty("MinimumPhotoCount")
 private String MinimumPhotoCount;
	@JsonProperty("OriginalPrice")
 private String OriginalPrice;
	@JsonProperty("Recommended")
 private String Recommended;
	
	Promotions(){
		
	}

 public String getName ()
 {
     return Name;
 }

 public void setName (String Name)
 {
     this.Name = Name;
 }

 public String getDescription ()
 {
     return Description;
 }

 public void setDescription (String Description)
 {
     this.Description = Description;
 }
 
 public String getOriginalPrice()
 {
     return OriginalPrice;
 }

 public void setOriginalPrice(String OriginalPrice)
 {
     this.OriginalPrice = OriginalPrice;
 }

 public String getRecommended()
 {
     return Recommended;
 }

 public void setRecommended(String Recommended)
 {
     this.Recommended = Recommended;
 }
 

 public String getPrice ()
 {
     return Price;
 }

 public void setPrice (String Price)
 {
     this.Price = Price;
 }

 public String getId ()
 {
     return Id;
 }

 public void setId (String Id)
 {
     this.Id = Id;
 }

 public String getMinimumPhotoCount ()
 {
     return MinimumPhotoCount;
 }

 public void setMinimumPhotoCount (String MinimumPhotoCount)
 {
     this.MinimumPhotoCount = MinimumPhotoCount;
 }

 @Override
 public String toString()
 {
     return "ClassPojo [Name = "+Name+", Description = "+Description+", Price = "+Price+", Id = "+Id+", MinimumPhotoCount = "+MinimumPhotoCount+"]";
 }
}
package org.yonus.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.yonus.pojo.Promotions;
import org.yonus.pojo.TestApi;





public class CommonUtils {

/**
 * This function get the APR url and returns the reponse.
 * @param URL
 * @return response
 */
public static Response getResponse(String URL) {
	Client client =  ClientBuilder.newClient();
	Response response = client.target(URL).request(MediaType.APPLICATION_JSON).get();
	return response;
}

/**
 * This function get the response and validates the response status
 * @param response
 * @return boolean
 */
public static boolean validateResponse(Response response) {
	if (response.getStatus() == 200)
		return true;
	else
		return false;
}
/**
 * This function response API as an object
 * @param response
 * @return
 * @throws JsonProcessingException
 * @throws IOException
 */
public static TestApi responseJsonNode (Response response) throws JsonProcessingException, IOException {
	ObjectMapper mapper = new ObjectMapper();
	JsonNode actualObj = mapper.readTree(response.readEntity(String.class));
	TestApi api = mapper.treeToValue(actualObj, TestApi.class);
	return api;
}



}

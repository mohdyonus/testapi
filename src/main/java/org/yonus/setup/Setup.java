package org.yonus.setup;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Setup {

	Properties defaultProps = new Properties();

	public String getURI() throws IOException {
		
		FileInputStream in = new FileInputStream("src/main/resources/Config.properties");
		defaultProps.load(in);
		return defaultProps.getProperty("URI");
		
	}

}

package org.yonus.steps;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.*;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;


import org.junit.runner.Description;
import org.yonus.pojo.Promotions;
import org.yonus.pojo.TestApi;
import org.yonus.setup.Setup;
import org.yonus.utilities.CommonUtils;



public class StepImplementation {

	CommonUtils cml = new CommonUtils();
	Setup config = new Setup();
	String URI = null;
	Response response = null;
	TestApi api = new TestApi();
	

	
	@Given("^The Categories Detail API$")
	public void the_Categories_Detail_API() throws IOException  {
	       URI = config.getURI();
	       assertTrue("API URL is Empty", URI.length() > 0);
	}

	@When("^Call the requested API get service$")
	public void call_the_requested_API_get_service() throws Throwable {
	    response = cml.getResponse(URI);
	    assertTrue("Not Received the response", response.hasEntity());
	}

	@When("^Returns the Successful response$")
	public void returns_the_Successful_response() throws Throwable {
	   
		assertTrue("Response Not matched ", cml.validateResponse(response));
	}

	@Then("^Validate API response have Element \"([^\"]*)\" as \"([^\"]*)\"$")
	public void validate_API_response_have_Element_as(String fieldName, String expectedvalue) throws Throwable {
		String apiValue = null;
		 api = cml.responseJsonNode(response);
	    if (fieldName.equals("Name"))
	    	apiValue = api.getName();
	    else if (fieldName.equals("CanRelist"))
	    	apiValue = api.getCanRelist();
	    assertTrue("Values not matched", (apiValue.trim().equals(expectedvalue.trim())));
	}

	@Then("^Validate API response have below Elements in \"([^\"]*)\"$")
	public void validate_API_response_have_below_Elements_in(String element, DataTable table) throws Throwable {
		 api = cml.responseJsonNode(response);
		boolean flag = false;
		List<Promotions> promotionList = new ArrayList<Promotions>();
		promotionList = api.getPromotions();
		
		for (Promotions promotion: promotionList) {
			flag = false;
			for (Map<String, String> data : table.asMaps(String.class, String.class)) {
				if (data.get("Field").equals("Name"))
					if (promotion.getName().equals(data.get("Value")))  {
						flag = true;
					}
				if (data.get("Field").equals("Description"))	
					if (promotion.getDescription().contains(data.get("Value")) && flag == true)  {
						System.out.println("Matched with" +promotion.toString());
					flag = true;
					break;
					}
			}
			if (flag == true) break;
		}
		assertTrue("Given Field and Values not matched with Any" + element, flag);
	}
	

}

# testapi
Validate Json API Response

This project to make a API Connection using Java Jax Rs and Validate the json output file elements.
I have acheived this using BDD, the given acceptance criteria is in BDD (Cucumber). Output will be produced in nice HTML format and can be 
understood by itself.


Build status

This is a Maven Based project and can be integreted with Jenkins etc.



Tech/framework used

I have used Maven project with BDD (Cucumber) & Junit, it can be built with Maven.


Code Example

Code is written in Java using Jax RS API. Common functions are available in utilities package which can be ported to any project.


public static Response getResponse(String URL) {

	Client client =  ClientBuilder.newClient();
	
	Response response = client.target(URL).request(MediaType.APPLICATION_JSON).get();
	
	return response;
} 

Installation

This project structure is setup based on Maven. Install Maven and clone this repo, do the maven build. All the dependencies will 
get downloaded by itself.

Tests

This feature Categories_Details_API_Validation validates below 3 Acceptance Criteria 

1. Name = "Carbon credits"
2. CanRelist = true
Above Acceptance criteria is testing in the feature Categories_Details_API_Validation. Feature is in src/test/resources/Categories_Details_API_Validation

@API

Scenario Outline: Validate the Fields and its value

	Given The Categories Detail API
	When Call the requested API get service
	And Returns the Successful response 
	Then Validate API response have Elements "<Fields>" as "<Value>"
	Examples:
	|Fields|Value|
	|Name|Carbon credits|
	|CanRelist|true|
	
3. The Promotions element with Name = "Gallery" has a Description that contains the text "2x larger image" is being testing below scneario.

Here All name & Description mached in Promotion node then the Validate API response have element will pass once all the Field & Values are mathed

@API	

Scenario:  Validate the Promotions

	Given The Categories Detail API
	When Call the requested API get service
	And Returns the Successful response 
	Then Validate API response have below Elements in "Promotions"
	|Field|Value|
	|Name|Gallery|
	|Description|2x larger image|


How to use?

All the tests should be run from Junit runner test. Goto src/test/java and run the Runner_Test class file as Junit.
This will intern call the Categories_Details._API_Validation.feature and execute the same. All the output gets generated in target
(target/Destination/index.html) - Nice HTML report will be shown about the acceptance criteria tested.




